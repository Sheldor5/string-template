package at.palata.string.template;

public enum LineSeparator {
    CR("\r"),
    LF("\n"),
    CRLF("\r\n"),
    NONE("");

    private final String value;

    LineSeparator(final String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}