package at.palata.string.template;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

public final class StringTemplate {

    private static final StringTemplateFactory DEFAULT_STRING_TEMPLATE_FACTORY = StringTemplateFactoryBuilder.builder().build();
    private final List<Function<Map<String, Object>, String>> fragments;
    private final int capacity;

    StringTemplate(final List<Function<Map<String, Object>, String>> fragments, final int chars, final int vars) {
        this.fragments = fragments;
        this.capacity = chars + (vars * 32);
    }

    public static StringTemplate of(final String template) {
        return DEFAULT_STRING_TEMPLATE_FACTORY.of(template);
    }

    public String render(final Map<String, Object> model) {
        final StringBuilder stringBuilder = new StringBuilder(this.capacity);
        for (final Function<Map<String, Object>, String> fragment : this.fragments) {
            stringBuilder.append(fragment.apply(model));
        }
        return stringBuilder.toString();
    }
}