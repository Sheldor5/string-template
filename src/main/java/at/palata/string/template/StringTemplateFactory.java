package at.palata.string.template;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public final class StringTemplateFactory {

    private final byte[] variablePrefixBytes;
    private final byte[] variablePostfixBytes;
    private final LineSeparator lineSeparator;
    private final Charset charset;

    StringTemplateFactory(final Charset charset, final String variablePrefix, final String variablePostfix, final LineSeparator lineSeparator) {
        this.variablePrefixBytes = variablePrefix.getBytes(charset);
        this.variablePostfixBytes = variablePostfix.getBytes(charset);
        this.lineSeparator = lineSeparator;
        this.charset = charset;
    }

    public static StringTemplateFactoryBuilder builder() {
        return new StringTemplateFactoryBuilder();
    }

    public StringTemplate of(final String template) {
        final Context context = new Context();
        final StringReader stringReader = new StringReader(template);
        final BufferedReader bufferedReader = new BufferedReader(stringReader);
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        final byte[] lineSeparator = this.getLineSeparator(template).getValue().getBytes(this.charset);

        byte[] lineContent;
        int lineContentLastIndex;
        try {
            String line = bufferedReader.readLine();
            while (line != null) {
                lineContent = line.getBytes(this.charset);
                lineContentLastIndex = lineContent.length - 1;
                for (int i = 0; i < lineContent.length; i++) {
                    if (isTokenBegin(lineContent, i, this.variablePrefixBytes)) {
                        i += this.variablePrefixBytes.length;
                        addFragment(byteArrayOutputStream, context);
                        for (; i < lineContent.length; i++) {
                            if (isTokenBegin(lineContent, i, this.variablePostfixBytes)) {
                                i += this.variablePostfixBytes.length - 1;
                                addVariable(byteArrayOutputStream, context);
                                break;
                            } else {
                                if (i == lineContentLastIndex) {
                                    throw new IllegalArgumentException();
                                }
                                byteArrayOutputStream.write(lineContent[i]);
                            }
                        }
                    } else {
                        byteArrayOutputStream.write(lineContent[i]);
                    }
                }
                line = bufferedReader.readLine();
                if (line != null) {
                    byteArrayOutputStream.write(lineSeparator);
                }
                addFragment(byteArrayOutputStream, context);
            }
        } catch (final IOException e) {
            throw new IllegalArgumentException(e);
        }

        return new StringTemplate(context.fragments, context.chars, context.vars);
    }

    private void addFragment(final ByteArrayOutputStream byteArrayOutputStream, final Context context) {
        if (byteArrayOutputStream.size() > 0) {
            final String partial = byteArrayOutputStream.toString(this.charset);
            byteArrayOutputStream.reset();
            context.chars += partial.length();
            context.fragments.add((map) -> partial);
        }
    }

    private void addVariable(final ByteArrayOutputStream byteArrayOutputStream, final Context context) {
        if (byteArrayOutputStream.size() > 0) {
            final String variable = byteArrayOutputStream.toString(this.charset);
            byteArrayOutputStream.reset();
            context.vars++;
            context.fragments.add((map) -> String.valueOf(map.get(variable)));
        }
    }

    private static boolean isTokenBegin(final byte[] content, final int offset, final byte[] token) {
        int end = offset + token.length;
        for (int i = offset, j = 0; i < end; i++, j++) {
            if (content[i] != token[j]) {
                return false;
            }
        }
        return true;
    }

    private LineSeparator getLineSeparator(final String template) {
        if (this.lineSeparator != null) {
            return this.lineSeparator;
        }
        for (int i = 0; i < template.length(); i++) {
            final char c = template.charAt(i);
            if (c == '\r') {
                final int j = i + 1;
                if (template.length() > j && template.charAt(j) == '\n') {
                    return LineSeparator.CRLF;
                } else {
                    return LineSeparator.CR;
                }
            } else if (c == '\n') {
                return LineSeparator.LF;
            }
        }
        return LineSeparator.NONE;
    }

    private static class Context {
        private final List<Function<Map<String, Object>, String>> fragments = new ArrayList<>(128);
        private int chars = 0;
        private int vars = 0;
    }
}