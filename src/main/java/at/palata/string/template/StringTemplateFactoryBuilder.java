package at.palata.string.template;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

final class StringTemplateFactoryBuilder {

    private Charset charset = StandardCharsets.UTF_8;
    private String variablePrefix = "${";
    private String variablePostfix = "}";
    private LineSeparator lineSeparator = null;

    static StringTemplateFactoryBuilder builder() {
        return new StringTemplateFactoryBuilder();
    }

    public StringTemplateFactoryBuilder withCharset(final Charset charset) {
        this.charset = Objects.requireNonNull(charset);
        return this;
    }

    public StringTemplateFactoryBuilder withVariablePrefix(final String variablePrefix) {
        this.variablePrefix = Objects.requireNonNull(variablePrefix);
        return this;
    }

    public StringTemplateFactoryBuilder withVariablePostfix(final String variablePostfix) {
        this.variablePostfix = Objects.requireNonNull(variablePostfix);
        return this;
    }

    public StringTemplateFactoryBuilder withLineSeparator(final LineSeparator lineSeparator) {
        this.lineSeparator = lineSeparator;
        return this;
    }

    public StringTemplateFactory build() {
        return new StringTemplateFactory(
                this.charset,
                this.variablePrefix,
                this.variablePostfix,
                this.lineSeparator
        );
    }
}