package at.palata.string.template;

import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class StringTemplateTest {

    @Test
    void test0() {
        final Map<String, Object> model = Map.of(
                "val", "test",
                "var", "This"
        );
        final String content = "${var} is just a ${val}!${val}";
        final StringTemplate stringTemplate = StringTemplate.of(content);

        final String actual = stringTemplate.render(model);

        assertEquals("This is just a test!test", actual);
    }

    @Test
    void test1() {
        final Map<String, Object> model = Map.of(
                "val", "???",
                "var", "What"
        );
        final String content = "${var} is just a ${val}!${val}";
        final StringTemplate stringTemplate = StringTemplate.of(content);

        final String actual = stringTemplate.render(model);

        assertEquals("What is just a ???!???", actual);
    }

    @Test
    void test2() {
        final Map<String, Object> model = Map.of(
                "value", "World"
        );
        final String content = "Hello @@value@@!";
        final StringTemplate stringTemplate = StringTemplateFactory.builder()
                .withVariablePrefix("@@")
                .withVariablePostfix("@@")
                .build()
                .of(content);

        final String actual = stringTemplate.render(model);

        assertEquals("Hello World!", actual);
    }

    @Test
    void test3() {
        final Map<String, Object> model = Map.of(
                "value", "World",
                "Hello", "Bye"
        );
        final String content = "[[Hello]] [[value]]";
        final StringTemplate stringTemplate = StringTemplateFactory.builder()
                .withVariablePrefix("[[")
                .withVariablePostfix("]]")
                .build()
                .of(content);

        final String actual = stringTemplate.render(model);

        assertEquals("Bye World", actual);
    }

    @Test
    void test4() {
        final Map<String, Object> model = Map.of(
                "value", "World",
                "name", "Michael",
                "age", "42",
                "signature", "Best Regards <3"
        );
        final String content = "Hello \\\\value//!\nMy name is \\\\name//.\rI am \\\\age// years old.\r\n\r\n\\\\signature//";
        final StringTemplate stringTemplate = StringTemplateFactory.builder()
                .withVariablePrefix("\\\\")
                .withVariablePostfix("//")
                .build()
                .of(content);

        final String actual = stringTemplate.render(model);

        assertEquals("Hello World!\nMy name is Michael.\nI am 42 years old.\n\nBest Regards <3", actual);
    }
}